﻿using Dapper;
using Microsoft.AspNetCore.Mvc;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using WebAp.Models;
using WebAp.Service;

namespace WebAp.Controllers
{
    public class CarsController : Controller
    {
        private readonly string ConnectionString = "User ID=postgres;Password=123;Host=localhost;Port=5432;Database=test123;";
        private IDbConnection con;
        private readonly ICarService _carService;

        public CarsController(ICarService carService)
        {
            _carService = carService;
        }

        public IActionResult Index()
        {
            var result = _carService.GetCar();
            return View("Index", result);
        }
        [HttpGet]
        public IActionResult Create() 
        {
            return View();
        }
        [HttpPost]
        public IActionResult Create(Car auto)
        {
            _carService.Create(auto);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
           var car = _carService.Update(id);
           
            return View(car);
        }
        [HttpPost]
        public IActionResult Update(Car car)
        {
            _carService.Update(car);
            return RedirectToAction(nameof(Index));
        }
        [HttpPost]
        public string Delete(int id)
        {
            return "OK";
        }
    }
}
