﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace WebAp.Models
{
    public class Car
    {
        
        public int Id { get; set; }

        [DisplayName("Марка")]
        public string Marka { get; set; }

        [DisplayName("Модель")]
        public string Model { get; set; }

        [DisplayName("Год выпуска")]
        public int GodVypuska { get; set; }
      
    }
}
