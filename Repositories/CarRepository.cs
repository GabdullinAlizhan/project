﻿using Dapper;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAp.Models;

namespace WebAp.Repositories
{
    public interface ICarRepository : IRepository
    {
        List<Car> GetAll();
        void Create(Car car);
        Car Update(int id);
        void Update(Car car);
        string Delete(int id);
    }

    public class CarRepository : Repository, ICarRepository
    {
        public CarRepository(IConnectionProvider connectionProvider)
      : base(connectionProvider)
        {

        }

        public List<Car> GetAll()
        {
            #region query

            var query = "SELECT * FROM auto WHERE isdeleted = false ORDER BY id DESC";

            #endregion

            using (var conn = GetConnection("MyConnectionString"))
            {
                var rslt = conn.Query<Car>(query).ToList();
                return rslt;
            }
            //try
            //{
            //    string selectQuery = "SELECT * FROM auto WHERE isdeleted = false";
            //    con = new NpgsqlConnection(ConnectionString);
            //    con.Open();
            //    IEnumerable<Car> listCars = con.Query<Car>(selectQuery).ToList();
            //    return View(listCars);
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}

        }
        public void Create(Car car)
        {
            #region query

            var query = "INSERT INTO auto(Marka, Model, GodVypuska) VALUES(@marka, @model, @godvypuska)";

            #endregion

            using (var conn = GetConnection("MyConnectionString"))
            {
                var rslt = conn.Execute(query,
                    new
                    {
                        marka = car.Marka,
                        model = car.Model,
                        godvypuska = car.GodVypuska
                    });
            }

        }
        public Car Update(int id)
        {
            #region query

            var query = "SELECT * FROM auto WHERE Id = @id";

            #endregion

            using (var conn = GetConnection("MyConnectionString"))
            {
                var rslt = conn.Query<Car>(query,
                    new
                    {

                        id = id
                    }).FirstOrDefault();
                return rslt;
            }


        }
        public void Update(Car car)
        {
            #region query

            var query = "UPDATE auto SET Marka = @marka, Model = @model, GodVypuska = @godvypuska WHERE Id = @id";

            #endregion

            using (var conn = GetConnection("MyConnectionString"))
            {
                var rslt = conn.Execute(query,
                    new
                    {
                        id = car.Id,
                        marka = car.Marka,
                        model = car.Model,
                        godvypuska = car.GodVypuska
                    });
            }

        }
        public string Delete(int id)
        {
            #region query

            var query = "UPDATE auto SET Marka = @marka, Model = @model, GodVypuska = @godvypuska WHERE Id = @id";

            #endregion

            using (var conn = GetConnection("MyConnectionString"))
            {
                var rslt = conn.Query(query,
                new
                {
                    id = id
                }).FirstOrDefault();
                return rslt;
            }
        }
    }
}