﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace WebAp
{
    #region Imterface

    public interface IConnectionProvider
    {
        IDbConnection OpenConnection(string name);
    }
    #endregion

    public class ConnectionProvider : IConnectionProvider
    {
        private readonly Dictionary<string, string> _idxConnections = new Dictionary<string, string>();

        public ConnectionProvider(IConfiguration cfg)
        {
            _idxConnections.Add("MyConnectionString", cfg["ConnectionStrings:MyConnectionString"]);
        }

        public IDbConnection OpenConnection(string name)
        {
            var rs = new NpgsqlConnection(_idxConnections[name]);
            return rs;
        }
    }
}