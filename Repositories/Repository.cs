﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace WebAp.Repositories
{
    public interface IRepository
    {
        IDbConnection GetConnection(string connectionname);
    }

    public abstract class Repository : IRepository
    {
        private readonly IConnectionProvider _connectionProvider;

        public Repository(IConnectionProvider connectionProvider)
        {
            _connectionProvider = connectionProvider;
        }

        public IDbConnection GetConnection(string connectionName)
        {
            return _connectionProvider.OpenConnection(connectionName);
        }
    }
}
