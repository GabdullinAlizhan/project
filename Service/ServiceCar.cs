﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAp.Models;
using WebAp.Repositories;

namespace WebAp.Service
{
    public interface ICarService
    {
        List<Car> GetCar();
        void Create(Car auto);
        Car Update(int id);
        void Update(Car car);
        string Delete(int id);
    }

    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public List<Car> GetCar()
        {
            var result = _carRepository.GetAll();
            return result;
        }
        public void Create(Car auto)
        {
            _carRepository.Create(auto);
        }

        public Car Update(int id)
        {
            var car = _carRepository.Update(id);
            return car;
        }
        public void Update(Car car)
        {
            _carRepository.Update(car);
        }
        public string Delete(int id)
        {
            var car = _carRepository.Delete(id);
            return car;
        }
    }
}
